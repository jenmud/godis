package cmd

import (
	"github.com/gobwas/glob"
	"sync"
	"time"
)

// NewKeyStore returns a new empty key value store.
func NewKeyStore() *KeyStore {
	store := new(KeyStore)
	store.keys = sync.Map{}
	return store
}

// KeyStore is a store for simple key and value pairs.
type KeyStore struct {
	keys sync.Map
}

// Set sets or overrides a key value pair.
func (ks *KeyStore) Set(key string, value interface{}) string {
	ks.keys.Store(key, value)
	return "OK"
}

// Get gets a key value pair.
func (ks *KeyStore) Get(key string) interface{} {
	value, ok := ks.keys.Load(key)

	if !ok {
		return "-1"
	}

	return value
}

// Exists returns 1 if the key exists else 0.
func (ks *KeyStore) Exists(key string) int {
	if _, ok := ks.keys.Load(key); ok {
		return 1
	}
	return 0
}

// Del deletes a key value pair.
func (ks *KeyStore) Del(keys ...string) int {
	deleted := 0
	for _, k := range keys {
		if ks.Exists(k) == 1 {
			ks.keys.Delete(k)
			deleted++
		}
	}
	return deleted
}

// Expire sets a expiry on the key in future.
func (ks *KeyStore) Expire(key string, TTL time.Duration) int {
	timerChan := time.After(TTL)
	go func() {
		<-timerChan
		ks.Del(key)
	}()
	return 1
}

// Keys returns all the keys in the store. Pattern should be in
// a glob formatting.
func (ks *KeyStore) Keys(pattern string) []string {
	globber := glob.MustCompile(pattern)
	keys := []string{}

	f := func(key, value interface{}) bool {
		k := key.(string)
		if globber.Match(k) {
			keys = append(keys, k)
		}
		return true
	}

	ks.keys.Range(f)
	return keys
}
