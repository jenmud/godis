// Copyright © 2018 Jenda Mudron <jenmud@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"log"
	"net"

	"github.com/spf13/cobra"
)

// Settings are the settings for configuring the server.
type Settings struct {
	// Addr is a network address endpoint
	Addr *net.TCPAddr
}

var settings Settings

// serverCmd represents the server command
var serverCmd = &cobra.Command{
	Use:   "server",
	Short: "Network service accepting client connections and commands.",
	Long: `
Start a network service accepting client connections and commands responding
with the appropriate responses.
	`,
	Run: func(cmd *cobra.Command, args []string) {
		ListenAndServe(settings)
	},
}

func init() {
	rootCmd.AddCommand(serverCmd)

	var address string
	var port int

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	serverCmd.Flags().StringVar(&address, "addr", "0.0.0.0", "Address to bind with")
	serverCmd.Flags().IntVar(&port, "port", 9000, "Port to listen and accept client connections on.")

	ip := net.ParseIP(address)
	settings.Addr = &net.TCPAddr{
		IP:   ip,
		Port: port,
	}
}

func ListenAndServe(settings Settings) error {
	listener, err := net.ListenTCP("tcp", settings.Addr)
	if err != nil {
		return err
	}

	for {
		conn, err := listener.Accept()

		if err != nil {
			return err
		}

		log.Println(conn.RemoteAddr().String())
	}

	return nil
}
