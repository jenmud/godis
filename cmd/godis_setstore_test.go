package cmd

import (
	"reflect"
	"testing"
)

func TestSetStore__SAdd(t *testing.T) {
	store := NewSetStore()
	output := store.SAdd("names", "Foo")
	assertIntEqual(t, 1, output)

	output = store.SAdd("names", "Bar", "Mickey")
	assertIntEqual(t, 2, output)

	expected := make(map[string]map[interface{}]struct{})
	expected["names"] = make(map[interface{}]struct{})
	expected["names"]["Bar"] = struct{}{}
	expected["names"]["Foo"] = struct{}{}
	expected["names"]["Mickey"] = struct{}{}

	if !reflect.DeepEqual(expected, store.keys) {
		t.Errorf("Expected %#v but got %#v", expected, store.keys)
	}
}

func TestSetStore__SRem(t *testing.T) {
	store := NewSetStore()
	store.SAdd("names", "Foo", "Bar", "Mickey")

	output := store.SRem("names", "Bar")
	assertIntEqual(t, 1, output)

	output = store.SRem("no such key", "Blah")
	assertIntEqual(t, 0, output)

	output = store.SRem("names", "no member")
	assertIntEqual(t, 0, output)

	store.SAdd("names", "Foo", "Bar", "Mickey")
	output = store.SRem("names", "Foo", "Mickey")
	assertIntEqual(t, 2, output)
}

func TestSetStore__SMembers(t *testing.T) {
	store := NewSetStore()
	store.SAdd("names", "Foo", "Bar", "Mickey")

	output := store.SMembers("names")
	expected := []interface{}{"Foo", "Bar", "Mickey"}
	assertStringCastItemsEqual(t, expected, output)

	output = store.SMembers("no such key")
	expected = []interface{}{}
	if !reflect.DeepEqual(expected, output) {
		t.Errorf("Expected %#v but got %#v", expected, output)
	}
}

func TestSetStore__SIsMember(t *testing.T) {
	store := NewSetStore()
	store.SAdd("names", "Foo", "Bar", "Mickey")

	output := store.SIsMember("names", "Bar")
	assertIntEqual(t, 1, output)

	output = store.SIsMember("names", "Blah")
	assertIntEqual(t, 0, output)

	output = store.SIsMember("no such key", "Blah")
	assertIntEqual(t, 0, output)
}

func TestSetStore__SUnion(t *testing.T) {
	store := NewSetStore()
	store.SAdd("names", "Foo", "Bar", "Mickey")
	store.SAdd("surnames", "Mouse", "Bar")

	output := store.SUnion("names", "surnames")
	expected := []interface{}{"Foo", "Bar", "Mickey", "Mouse"}

	assertStringCastItemsEqual(t, expected, output)
}

func TestSetStore__SUnionStore(t *testing.T) {
	store := NewSetStore()
	store.SAdd("names", "Foo", "Bar", "Mickey")
	store.SAdd("surnames", "Mouse", "Bar")

	output := store.SUnionStore("all", "names", "surnames")
	expected := []interface{}{"Foo", "Bar", "Mickey", "Mouse"}
	assertIntEqual(t, 4, output)

	members := store.SMembers("all")
	assertStringCastItemsEqual(t, expected, members)
}

func TestSetStore__SInter(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c")
	store.SAdd("key2", "c", "d", "e")
	store.SAdd("key3", "a", "c", "e")
	store.SAdd("key4", "a", "e", "d")

	output := store.SInter("key1", "key2", "key3")
	expected := []interface{}{"c"}
	assertStringCastItemsEqual(t, expected, output)
	// Test for failure
	output = store.SInter("key1", "key2", "key3", "key4")
	assertIntEqual(t, 0, len(output))
}

func TestSetStore__SInterStore(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c")
	store.SAdd("key2", "c", "d", "e")
	store.SAdd("key3", "a", "c", "e")

	output := store.SInterStore("key4", "key1", "key2", "key3")
	expected := []interface{}{"c"}
	assertIntEqual(t, 1, output)
	assertStringCastItemsEqual(t, expected, store.SMembers("key4"))

	// this should override key4 with a new set
	output = store.SInterStore("key4", "key1", "key3")
	expected = []interface{}{"a", "c"}
	assertIntEqual(t, 2, output)
	assertStringCastItemsEqual(t, expected, store.SMembers("key4"))
}

func TestSetStore__SDiff(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c", "d")
	store.SAdd("key2", "c")
	store.SAdd("key3", "a", "c", "e")
	store.SAdd("key4", "a", "c", "e", "f")

	output := store.SDiff("key1", "key2", "key3", "key4")
	expected := []interface{}{"b", "d"}
	assertStringCastItemsEqual(t, expected, output)
}

func TestSetStore__SDiffStore(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c", "d")
	store.SAdd("key2", "c")
	store.SAdd("key3", "a", "c", "e")

	output := store.SDiffStore("key4", "key1", "key2", "key3")
	assertIntEqual(t, 2, output)

	members := store.SMembers("key4")
	expected := []interface{}{"b", "d"}
	assertStringCastItemsEqual(t, expected, members)
}

func TestSetStore__SCard(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c", "d")

	output := store.SCard("key1")
	assertIntEqual(t, 4, output)

	output = store.SCard("key-missing")
	assertIntEqual(t, 0, output)
}

func TestSetStore__SMove(t *testing.T) {
	// Run the Redis example as a test
	store := NewSetStore()
	store.SAdd("key1", "a", "b", "c", "d")
	store.SAdd("key2", "c")

	output := store.SMove("key1", "key2", "b")
	assertIntEqual(t, 1, output)
	assertIntEqual(t, 0, store.SIsMember("key1", "b"))
	assertIntEqual(t, 1, store.SIsMember("key2", "b"))

	output = store.SMove("key1", "key2", "no-such-member")
	assertIntEqual(t, 0, output)
	assertIntEqual(t, 0, store.SIsMember("key2", "no-such-member"))
}
