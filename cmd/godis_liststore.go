package cmd

import (
	"fmt"
	"sync"
)

// NewListStore returns a new empty list store.
func NewListStore() *ListStore {
	store := new(ListStore)
	store.keys = make(map[string][]interface{})
	return store
}

// ListStore is a list store.
type ListStore struct {
	lock sync.RWMutex
	keys map[string][]interface{}
}

// RPush insert value at the tail end of the list.
func (ls *ListStore) RPush(key string, value ...interface{}) int {
	ls.lock.Lock()
	defer ls.lock.Unlock()

	var list []interface{}

	list, ok := ls.keys[key]
	if !ok {
		ls.keys[key] = list
	}

	for _, v := range value {
		ls.keys[key] = append(ls.keys[key], v)
	}

	return len(ls.keys[key])
}

// RPop return and remove the last element in the list.
func (ls *ListStore) RPop(key string) interface{} {
	ls.lock.Lock()
	defer ls.lock.Unlock()

	_, ok := ls.keys[key]
	if !ok {
		return nil
	}

	length := len(ls.keys[key])
	item := ls.keys[key][length-1]
	ls.keys[key] = append(ls.keys[key][:length-1], ls.keys[key][length:]...)
	return item
}

// LPush inserts value at the head of the list.
func (ls *ListStore) LPush(key string, value ...interface{}) int {
	ls.lock.Lock()
	defer ls.lock.Unlock()

	var list []interface{}

	list, ok := ls.keys[key]
	if !ok {
		ls.keys[key] = list
	}

	for _, v := range value {
		ls.keys[key] = append([]interface{}{v}, ls.keys[key]...)
	}

	return len(ls.keys[key])
}

// LPop return and remove the first element in the list.
func (ls *ListStore) LPop(key string) interface{} {
	ls.lock.Lock()
	defer ls.lock.Unlock()

	_, ok := ls.keys[key]
	if !ok {
		return nil
	}

	item := ls.keys[key][0]
	ls.keys[key] = append(ls.keys[key][1:1], ls.keys[key][1:]...)
	return item
}

// LRange returns items stored at key.
func (ls *ListStore) LRange(key string, start, end int) []interface{} {
	ls.lock.RLock()
	defer ls.lock.RUnlock()

	storedItems, ok := ls.keys[key]
	length := len(storedItems)
	if !ok {
		return []interface{}{}
	}

	if start < 0 {
		start = length + start
		if start < 0 {
			start = 0
		}
	}

	if start > length {
		start = length
	}

	if end < 0 {
		end = length + end
		if end < 0 {
			end = -1
		}
	}
	end++

	if end > length {
		end = length
	}

	items := []interface{}{}
	for _, item := range storedItems[start:end] {
		items = append(items, item)
	}

	return items
}

// LLen returns the length of all the items in the store.
func (ls *ListStore) LLen(key string) int {
	ls.lock.RLock()
	defer ls.lock.RUnlock()

	items, ok := ls.keys[key]
	if !ok {
		return 0
	}
	return len(items)
}

// LIndex returns the element at index `index` in the list store.
func (ls *ListStore) LIndex(key string, index int) (interface{}, error) {
	ls.lock.RLock()
	defer ls.lock.RUnlock()

	items, ok := ls.keys[key]
	if !ok {
		return nil, fmt.Errorf("No such key %s", key)
	}

	var length = len(items)
	if index < 0 {
		index = -index
		i := length - index
		if index > length {
			index = 0
			i = 0
		}
		return items[i], nil
	}

	return items[index], nil
}
