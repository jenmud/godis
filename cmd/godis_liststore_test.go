package cmd

import (
	"reflect"
	"testing"
)

func TestListStore__RPush(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "Foo", "Bar")
	output := store.RPush("names", "Mickey")

	assertIntEqual(t, 3, output)

	storedItems := store.keys["names"]
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}

	expected := []string{"Foo", "Bar", "Mickey"}
	if !reflect.DeepEqual(expected, items) {
		t.Errorf("List order do not match, expected %s but got %s", expected, items)
	}
}

func TestListStore__RPop(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "Foo", "Bar", "Mickey")
	output := store.RPop("names")

	storedItems := store.keys["names"]
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}

	assertIntEqual(t, 2, len(storedItems))
	assertStringEqual(t, "Mickey", output.(string))

	expected := []string{"Foo", "Bar"}
	if !reflect.DeepEqual(expected, items) {
		t.Errorf("List order do not match, expected %s but got %s", expected, items)
	}
}

func TestListStore__LPush(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "Foo")
	output := store.LPush("names", "Bar")

	assertIntEqual(t, 2, output)

	storedItems := store.keys["names"]
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}

	expected := []string{"Bar", "Foo"}
	if !reflect.DeepEqual(expected, items) {
		t.Errorf("List order do not match, expected %s but got %s", expected, items)
	}
}

func TestListStore__LPop(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "Foo", "Bar", "Mickey")
	output := store.LPop("names")

	storedItems := store.keys["names"]
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}

	assertIntEqual(t, 2, len(storedItems))
	assertStringEqual(t, "Foo", output.(string))

	expected := []string{"Bar", "Mickey"}
	if !reflect.DeepEqual(expected, items) {
		t.Errorf("List order do not match, expected %s but got %s", expected, items)
	}

	store = NewListStore()
	store.RPush("names", "Foo")
	output = store.LPop("names")
	assertIntEqual(t, 0, len(store.keys["names"]))
	assertStringEqual(t, "Foo", output.(string))
}

func TestListStore__LRange(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "one", "two", "three")

	storedItems := store.LRange("names", 0, 0)
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{"one"}, items)

	storedItems = store.LRange("names", 1, 2)
	items = make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{"two", "three"}, items)

	storedItems = store.LRange("names", -3, 2)
	items = make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{"one", "two", "three"}, items)

	storedItems = store.LRange("names", -100, 100)
	items = make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{"one", "two", "three"}, items)

	storedItems = store.LRange("names", 5, 10)
	items = make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{}, items)
}

func TestListStore__LRange__no_key(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "one", "two", "three")

	storedItems := store.LRange("surnames", 0, 0)
	items := make([]string, len(storedItems))
	for i, item := range storedItems {
		items[i] = item.(string)
	}
	assertStringItemsEqual(t, []string{}, items)
}

func TestListStore__LLen(t *testing.T) {
	store := NewListStore()
	store.RPush("names", "one", "two", "three")
	output := store.LLen("names")
	assertIntEqual(t, 3, output)

	output = store.LLen("no such key")
	assertIntEqual(t, 0, output)
}

func TestListStore__LIndex(t *testing.T) {
	store := NewListStore()
	store.LPush("names", "World")
	store.LPush("names", "Hello")

	output, err := store.LIndex("names", 0)
	assertNotNil(t, err)
	assertStringEqual(t, "Hello", output.(string))

	output, err = store.LIndex("names", 1)
	assertNotNil(t, err)
	assertStringEqual(t, "World", output.(string))

	output, err = store.LIndex("names", -1)
	assertNotNil(t, err)
	assertStringEqual(t, "World", output.(string))

	output, err = store.LIndex("names", -100)
	assertNotNil(t, err)
	assertStringEqual(t, "Hello", output.(string))
}
