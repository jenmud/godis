package cmd

import (
	"sync"
)

// NewSetStore returns a new empty set store.
func NewSetStore() *SetStore {
	store := new(SetStore)
	store.keys = make(map[string]map[interface{}]struct{})
	return store
}

// SetStore is a set store.
type SetStore struct {
	lock sync.RWMutex
	keys map[string]map[interface{}]struct{}
}

// SAdd adds or updates a value in the set store.
func (ss *SetStore) SAdd(key string, value ...interface{}) int {
	ss.lock.Lock()
	defer ss.lock.Unlock()
	inserted := 0
	for _, v := range value {
		if _, ok := ss.keys[key]; !ok {
			ss.keys[key] = make(map[interface{}]struct{})
		}
		ss.keys[key][v] = struct{}{}
		inserted++
	}
	return inserted
}

// SRem the specified member/s from the set.
func (ss *SetStore) SRem(key string, member ...interface{}) int {
	ss.lock.Lock()
	defer ss.lock.Unlock()

	_, ok := ss.keys[key]
	if !ok {
		return 0
	}

	removed := 0
	for _, m := range member {
		if _, ok := ss.keys[key][m]; ok {
			delete(ss.keys[key], m)
			removed++
		}
	}

	return removed
}

// SMembers returns all the members for `key` in the set.
func (ss *SetStore) SMembers(key string) []interface{} {
	ss.lock.RLock()
	defer ss.lock.RUnlock()

	itemKey, ok := ss.keys[key]
	if !ok {
		return []interface{}{}
	}

	index := 0
	members := make([]interface{}, len(itemKey))

	for k, _ := range itemKey {
		members[index] = k
		index++
	}

	return members
}

// SIsMember returns 1 if `member` is in the set or 0 if not.
func (ss *SetStore) SIsMember(key string, member interface{}) int {
	ss.lock.RLock()
	defer ss.lock.RUnlock()

	itemKeys, ok := ss.keys[key]
	if !ok {
		// main key does not exist
		return 0
	}

	_, ok = itemKeys[member]
	switch ok {
	case true:
		return 1
	default:
		return 0
	}
}

// SUnion returns members of the set resulting from the union of all given sets.
func (ss *SetStore) SUnion(key ...string) []interface{} {
	ss.lock.RLock()
	defer ss.lock.RUnlock()
	union := []interface{}{}

	members := make(map[interface{}]struct{})
	for _, k := range key {
		for _, member := range ss.SMembers(k) {
			if _, ok := members[member]; ok {
				continue
			}
			union = append(union, member)
			members[member] = struct{}{}
		}
	}

	return union
}

// SUnionStore like SUnion but stores the union as the members
// of the `dest` key.
func (ss *SetStore) SUnionStore(dest string, key ...string) int {
	union := ss.SUnion(key...)
	return ss.SAdd(dest, union...)
}

// SInter returns the members of the set resulting from the
// intersection of all the given sets.
func (ss *SetStore) SInter(key ...string) []interface{} {
	ss.lock.RLock()
	defer ss.lock.RUnlock()

	union := ss.SUnion(key...)

	for _, k := range key {
		// matched is use to tell whether we have exhausted a set
		// and there are not matches.
		matched := false
		for index, member := range union {
			length := index + 1
			if ss.SIsMember(k, member) == 0 {
				if length > len(union) {
					length = len(union)
				}
				union = append(union[:length-1], union[length:]...)
				continue
			}
			matched = true
		}

		// The set has not members, so this invalidates the intersection
		if !matched {
			return []interface{}{}
		}
	}

	return union
}

// SInterStore is like `Sinter` but stores the results of the
// intersection as the members of the destination key.
func (ss *SetStore) SInterStore(dest string, key ...string) int {
	intersection := ss.SInter(key...)
	return ss.SAdd(dest, intersection...)
}

// SDiff returns the members of the set resulting from the difference
// between the first set and all the successive sets.
func (ss *SetStore) SDiff(key string, keys ...string) []interface{} {
	ss.lock.RLock()
	defer ss.lock.RUnlock()

	// setA {a, b, c, d}
	// setB {c}
	// setC {a, c, e}
	// union -> {a, b, c, d, e}

	// SetA a in setB {c} -> SetA {a, b, d}
	// SetA a in setC {a, c, e} -> SetA {b, d}
	diff := ss.SMembers(key)

	for _, k := range keys {
		for index, member := range diff {
			if ss.SIsMember(k, member) == 1 {
				diff = append(diff[:index], diff[index+1:]...)
			}
		}
	}

	return diff
}

// SDiffStore like SDiff but stores the difference in a new key.
func (ss *SetStore) SDiffStore(dest string, key string, keys ...string) int {
	diff := ss.SDiff(key, keys...)
	return ss.SAdd(dest, diff...)
}

// SCard returns the numbers of members in key.
func (ss *SetStore) SCard(key string) int {
	set, ok := ss.keys[key]
	if !ok {
		return 0
	}
	return len(set)
}

// SMove moves a member from source to destination set.
func (ss *SetStore) SMove(src, dest string, member interface{}) int {
	ss.lock.RLock()

	set, ok := ss.keys[src]
	if !ok {
		return 0
	}

	_, ok = set[member]
	if !ok {
		return 0
	}

	ss.lock.RUnlock()

	ss.SAdd(dest, member)
	ss.SRem(src, member)
	return 1
}
