package cmd

import (
	"reflect"
	"sort"
	"testing"
	"time"
)

func sortStringItems(items []string) {
	sort.Strings(items)
}

func assertNotNil(t *testing.T, got interface{}) {
	if got != nil {
		t.Errorf("Expected nil but got %s", got)
	}
}

func assertStringEqual(t *testing.T, expected, got string) {
	if got != expected {
		t.Errorf("Expected %s but got %s", expected, got)
	}
}

func assertIntEqual(t *testing.T, expected, got int) {
	if got != expected {
		t.Errorf("Expected %d but got %d", expected, got)
	}
}

func assertStringItemsEqual(t *testing.T, expected, got []string) {
	sortStringItems(expected)
	sortStringItems(got)

	if !reflect.DeepEqual(expected, got) {
		t.Errorf("Expected %v but got %v", expected, got)
	}
}

func assertStringCastItemsEqual(t *testing.T, expected, got []interface{}) {
	expectedConv := make([]string, len(expected))
	for i, v := range expected {
		expectedConv[i] = v.(string)
	}

	gotConv := make([]string, len(got))
	for i, v := range got {
		gotConv[i] = v.(string)
	}

	assertStringItemsEqual(t, expectedConv, gotConv)
}

func TestKeyStore__Set(t *testing.T) {
	store := NewKeyStore()
	output := store.Set("name", "Foo")
	expected := "OK"

	assertStringEqual(t, expected, output)
	assertIntEqual(t, 1, store.Exists("name"))
}

func TestKeyStore__Get(t *testing.T) {
	store := NewKeyStore()
	store.Set("name", "Foo")

	output := store.Get("name").(string)
	expected := "Foo"

	assertStringEqual(t, expected, output)
}

func TestKeyStore__Get__not_found(t *testing.T) {
	store := NewKeyStore()

	output := store.Get("name").(string)
	expected := "-1"

	assertStringEqual(t, expected, output)
}

func TestKeyStore__Del(t *testing.T) {
	store := NewKeyStore()
	store.Set("name", "Foo")
	store.Set("surname", "Bar")
	store.Set("age", 21)

	output := store.Del("name", "surname")
	expected := 2

	assertIntEqual(t, expected, output)
	assertIntEqual(t, 0, store.Exists("surname"))
	assertIntEqual(t, 0, store.Exists("name"))
	assertIntEqual(t, 1, store.Exists("age"))
}

func TestKeyStore__Expire(t *testing.T) {
	store := NewKeyStore()
	store.Set("name", "Foo")
	store.Set("surname", "Bar")
	store.Set("age", 21)

	output := store.Expire("name", 2*time.Second)
	expected := 1
	assertIntEqual(t, expected, output)
	assertIntEqual(t, 1, store.Exists("name"))

	timeChan := time.After(3 * time.Second)
	<-timeChan
	assertIntEqual(t, 0, store.Exists("name"))
	assertIntEqual(t, 1, store.Exists("surname"))
	assertIntEqual(t, 1, store.Exists("age"))
}

func TestKeyStore__Exists(t *testing.T) {
	store := NewKeyStore()
	store.Set("name", "Foo")

	output := store.Exists("name")
	expected := 1
	assertIntEqual(t, expected, output)
}

func TestKeyStore__Exists__does_not_exists(t *testing.T) {
	store := NewKeyStore()

	output := store.Exists("name")
	expected := 0
	assertIntEqual(t, expected, output)
}

func TestKeyStore__Keys(t *testing.T) {
	store := NewKeyStore()
	store.Set("person:name", "Foo")
	store.Set("person:surname", "Bar")
	store.Set("person:age", 21)

	output := store.Keys("person:name")
	expected := []string{"person:name"}
	assertIntEqual(t, 1, len(output))
	assertStringItemsEqual(t, expected, output)

	output = store.Keys("person:?ame")
	expected = []string{"person:name"}
	assertIntEqual(t, 1, len(output))
	assertStringItemsEqual(t, expected, output)

	output = store.Keys("person:*")
	expected = []string{"person:name", "person:surname", "person:age"}
	assertIntEqual(t, 3, len(output))
	assertStringItemsEqual(t, expected, output)

	output = store.Keys("*name*")
	expected = []string{"person:name", "person:surname"}
	assertIntEqual(t, 2, len(output))
	assertStringItemsEqual(t, expected, output)

	store.Set("hello", "hello-value")
	store.Set("hallo", "hallo-value")
	store.Set("hollo", "hallo-value")
	output = store.Keys("h[ae]llo")
	expected = []string{"hello", "hallo"}
	assertStringItemsEqual(t, expected, output)
}
